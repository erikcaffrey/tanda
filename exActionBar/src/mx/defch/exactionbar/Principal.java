package mx.defch.exactionbar;

import mx.defch.exaction.enm.MenuOptionType;
import mx.defch.exaction.model.MenuOption;
import mx.defch.exactionbar.adt.ListMenuOptionsAdapter;
import mx.defch.exactionbar.utl.Util;
import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

@SuppressLint("NewApi")
public class Principal extends ActionBarActivity {

	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerLayout mDrawerLayout;
	private LinearLayout mDrawerLeft;
	
	private ListView mDrawerList;
	
	private ListMenuOptionsAdapter listOptionsAdapter;
	private CharSequence mDrawerTitle;
	private CharSequence mTitle;
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		android.app.ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#424242")));

		setContentView(R.layout.activity_exm_main);
		try {
			initDraw();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void initDraw() throws Exception {
		mTitle = mDrawerTitle = getTitle();
		if (mDrawerLayout == null) {
			mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
			setOptions();
		}
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer,0,0) {
			public void onDrawerClosed(View view) {
				getSupportActionBar().setTitle(mTitle);
			}

			public void onDrawerOpened(View drawerView) {
				getSupportActionBar().setTitle(mDrawerTitle);

			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		mDrawerToggle.syncState();
	}
	
	private void setOptions() throws Exception{
		View left = findViewById(R.id.menuOptions);
		if (left != null && left instanceof LinearLayout) {
			mDrawerLeft = (LinearLayout) left;

			mDrawerList = (ListView) mDrawerLeft
					.findViewById(R.id.menuOptionsList);
			initListItems();
			mDrawerList
			.setOnItemClickListener(new DrawerItemClickListener());
	mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
			GravityCompat.START);
	getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	getSupportActionBar().setHomeButtonEnabled(true);
		}
	}
	
	private void initListItems(){
		listOptionsAdapter = new ListMenuOptionsAdapter(this);
		for (MenuOptionType type : MenuOptionType.values()) {
			MenuOption opt = new MenuOption();
			opt.setType(type);
			opt.setTitle(Util.getResourceString(this, type.getTextResource()));
			opt.setResourceId(R.layout.home_options_item);
			listOptionsAdapter.add(opt);
		}
		mDrawerList.setAdapter(listOptionsAdapter);
	}
	
	private class DrawerItemClickListener implements
	ListView.OnItemClickListener {
@Override
public void onItemClick(AdapterView<?> parent, View view, int position,
		long id) {
	selectItem(position);
}
}
	
	private void selectItem(int position) {
		MenuOption option = listOptionsAdapter.getItem(position);
		try {
			switch (option.getType()) {
			case FgOne:
			case FgTwo:
			case FgTrhree:
			case FgFour:
				setFragment(option);
				break;
			default:
				break;
			}
			closeLeftDrawer();
		} catch (Exception e) {
			Log.e("ERROR", "ERROR SpmMain - selectItem :\n" + e.getMessage());
		}
		mDrawerList.setItemChecked(position, true);
	}
	
	public void setFragment(MenuOption option) throws Exception {
		setFragment(option.getTitle(), option.getType().getFragment());
	}
	
	public void setFragment(String title, Class<? extends Fragment> frg)
			throws Exception {
		try {
			setFragment(title, frg, false);
		} catch (Exception e) {
			throw new Exception("ERROR Home - setHomeFragmen : "
					+ e.getMessage());
		}
	}
	
	public void setFragment(String title, Class<? extends Fragment> frg,
			boolean stacked) throws Exception {
		try {
			if (mDrawerLayout != null) {
				mDrawerLayout
						.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
			}
			Util.setFragmentWorkspace(false, getSupportFragmentManager(),
					frg, stacked);
			setTitle(title);
		} catch (Exception e) {
			throw new Exception("ERROR Home - setHomeFragmen : "
					+ e.getMessage());
		}
	}
	
	
	private void closeLeftDrawer() {
		mDrawerLayout.closeDrawer(mDrawerLeft);
	}
	
	@Override
	public void onBackPressed() {
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle != null && mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.exm_main, menu);
		return true;
	}

}
