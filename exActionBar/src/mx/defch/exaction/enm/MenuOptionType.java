package mx.defch.exaction.enm;


import java.util.EnumSet;

import mx.defch.exactionbar.R;
import mx.defch.exactionbar.frg.Fragment_four;
import mx.defch.exactionbar.frg.Fragment_one;
import mx.defch.exactionbar.frg.Fragment_three;
import mx.defch.exactionbar.frg.Fragment_two;
import android.support.v4.app.Fragment;
import android.util.SparseArray;


public enum MenuOptionType {

	FgOne(1, R.string.f1, 0, Fragment_one.class), 
	FgTwo(2, R.string.f2, 0 , Fragment_two.class),
	FgTrhree(3, R.string.f3, 0 , Fragment_three.class),
	FgFour(4, R.string.f4, 0 , Fragment_four.class);
	private int code;
	private int textResource;
	private int iconResource;
	Class<? extends Fragment> fragment;

	private static final SparseArray<MenuOptionType> lookup = new SparseArray<MenuOptionType>();

	static {
		for (MenuOptionType s : EnumSet.allOf(MenuOptionType.class)) {
			lookup.put(s.getCode(), s);
		}
	}

	MenuOptionType(int code, int textResource, int iconResource,
			Class<? extends Fragment> fragment) {
		this.code = code;
		this.textResource = textResource;
		this.iconResource = iconResource;
		this.fragment = fragment;
	}

	public int getCode() {
		return code;
	}

	public static MenuOptionType get(int code) {
		return lookup.get(code);
	}

	public int getTextResource() {
		return textResource;
	}

	public int getIconResource() {
		return iconResource;
	}

	public Class<? extends Fragment> getFragment() {
		return fragment;
	}

	public void setFragment(Class<? extends Fragment> fragment) {
		this.fragment = fragment;
	}

	


}
